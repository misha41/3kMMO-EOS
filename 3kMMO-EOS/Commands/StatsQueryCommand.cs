﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using Epic.OnlineServices.Stats;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class StatsQueryCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.ProductUserId);
        }

        public override void Execute(object parameter)
        {
            ViewModelLocator.Stats.Stats = new ObservableCollection<Stat>();
            StatsService.Query();
        }
    }
}
