﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Models;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class FriendsQueryCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.AccountId);
        }

        public override void Execute(object parameter)
        {
            ViewModelLocator.Friends.Friends = new ObservableCollection<Friend>();
            FriendsService.QueryFriends();
        }
    }
}
