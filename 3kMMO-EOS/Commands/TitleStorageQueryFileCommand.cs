﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using Epic.OnlineServices.TitleStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class TitleStorageQueryFileCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.AccountId) && !string.IsNullOrWhiteSpace(ViewModelLocator.TitleStorage
    .TitleStorageFileName);
        }

        public override void Execute(object parameter)
        {
            ViewModelLocator.TitleStorage.TitleStorageFiles = new ObservableCollection<FileMetadata>();
            TitleStorageService.QueryFile(ViewModelLocator.TitleStorage
    .TitleStorageFileName);
        }
    }
}
