﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class StatsClickCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.ProductUserId);
        }

        public override void Execute(object parameter)
        {
            ViewModelLocator.Stats.Clicks++;
        }
    }
}
