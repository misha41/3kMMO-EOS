﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class AuthLoginCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return string.IsNullOrWhiteSpace(ViewModelLocator.Main.AccountId);
        }

        public override void Execute(object parameter)
        {
            AuthService.AuthLogin();
        }
    }
}
