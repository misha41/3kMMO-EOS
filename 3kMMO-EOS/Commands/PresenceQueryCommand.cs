﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Models;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using Epic.OnlineServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class PresenceQueryCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            if (parameter == null)
                return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.AccountId);
            else
                return ((Friend)parameter).EpicAccountId != null;
        }

        public override void Execute(object parameter)
        {
            if (parameter == null)
                PresenceService.Copy(EpicAccountId
        .FromString(ViewModelLocator.Main.AccountId), EpicAccountId.FromString(ViewModelLocator.Main.AccountId));
            else
                PresenceService.Query(((Friend)parameter).EpicAccountId);
        }
    }
}
