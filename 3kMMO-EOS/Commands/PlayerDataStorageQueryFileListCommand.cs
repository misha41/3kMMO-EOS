﻿using _3kMMO_EOS.Helpers;
using _3kMMO_EOS.Services;
using _3kMMO_EOS.ViewModels;
using Epic.OnlineServices.PlayerDataStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace _3kMMO_EOS.Commands
{
    public class PlayerDataStorageQueryFileListCommand : CommandBase
    {
        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(ViewModelLocator.Main.ProductUserId);
        }

        public override void Execute(object parameter)
        {
            ViewModelLocator.PlayerDataStorage.PlayerDataStorageFiles = new ObservableCollection<FileMetadata>();
            PlayerDataStorageService.QueryFileList();
        }
    }
}
