﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace _3kMMO_EOS.Helpers
{
    public class CommandBase : ICommand
    {
        public virtual bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }   

        public virtual void Execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }
    }
}
