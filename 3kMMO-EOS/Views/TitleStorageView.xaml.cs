﻿using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3kMMO_EOS.Views
{
    /// <summary>
    /// Interaction logic for TitleStorageView.xaml
    /// </summary>
public partial class TitleStorageView : UserControl
{
    public TitleStorageViewModel ViewModel { get { return ViewModelLocator.TitleStorage; } }

    public TitleStorageView()
    {
        InitializeComponent();
        DataContext = ViewModel;
    }

    private void TitleStorageFilesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
            ViewModel.TitleStorageReadFile.RaiseCanExecuteChanged();
        }

        private void TitleStorageFileNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        ViewModel.TitleStorageQueryFile.RaiseCanExecuteChanged();
    }

    private void TitleStorageTagTextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        ViewModel.TitleStorageQueryFileList.RaiseCanExecuteChanged();
    }
}
}
