﻿using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3kMMO_EOS.Views
{
    /// <summary>
    /// Interaction logic for PlayerDataStorageView.xaml
    /// </summary>
    public partial class PlayerDataStorageView : UserControl
    {
        public PlayerDataStorageViewModel ViewModel { get { return ViewModelLocator.PlayerDataStorage; } }

        public PlayerDataStorageView()
        {
            InitializeComponent();
            DataContext = ViewModel;
        }

        private void PlayerDataStorageFilesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.PlayerDataStorageReadFile.RaiseCanExecuteChanged();
            ViewModel.PlayerDataStorageDuplicateFile.RaiseCanExecuteChanged();
            ViewModel.PlayerDataStorageWriteFile.RaiseCanExecuteChanged();
            ViewModel.PlayerDataStorageDeleteFile.RaiseCanExecuteChanged();
        }
    }
}
