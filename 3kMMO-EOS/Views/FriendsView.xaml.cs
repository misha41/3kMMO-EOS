﻿using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3kMMO_EOS.Views
{
    /// <summary>
    /// Interaction logic for FriendsView.xaml
    /// </summary>
    public partial class FriendsView : UserControl
    {
        public FriendsViewModel ViewModel { get { return ViewModelLocator.Friends; } }

        public FriendsView()
        {
            InitializeComponent();
            DataContext = ViewModel;
        }

        private void FriendsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.PresenceQuery.RaiseCanExecuteChanged();
        }
    }
}
