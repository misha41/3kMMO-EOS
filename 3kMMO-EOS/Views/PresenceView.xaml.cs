﻿using _3kMMO_EOS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3kMMO_EOS.Views
{
    /// <summary>
    /// Interaction logic for PresenceView.xaml
    /// </summary>
    public partial class PresenceView : UserControl
    {
        public PresenceViewModel ViewModel { get { return ViewModelLocator.Presence; } }

        public PresenceView()
        {
            InitializeComponent();
            DataContext = ViewModel;
        }
    }
}
