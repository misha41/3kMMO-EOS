﻿using Epic.OnlineServices;
using Epic.OnlineServices.Auth;
using Epic.OnlineServices.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace _3kMMO_EOS.SDK
{
    public class ApplicationSettings
    {

        public string EncryptionKey = "1111111111111111111111111111111111111111111111111111111111111111";
        public string CacheDirectory = Path.GetTempPath();
        public string ProductId { get; private set; } = "4ea2db9734984583a964e2a39fd5220f";

        public string ClientId { get; private set; } = "xyza7891260Qry9WavjoNrAcQ8GeC3UG";
        public string ClientSecret { get; private set; } = "rmcokwr4DofEDqQPpbEpCDhc0OqR1QVvbUDvDI3rbPU";

        public string SandboxId { get; private set; } = "b45fca31650b4f7ab4f60aca1171d380";

        public string DeploymentId { get; private set; } = "38679053415c4019af276f734fd5c34f";

        public PlatformInterface PlatformInterface { get; set; }


        public LoginCredentialType LoginCredentialType { get; private set; } = LoginCredentialType.AccountPortal;
        public string Id { get; private set; } = "fghi4567knZKfO7N9XtnGHa30B1s6uDQ";
        public string Token { get; private set; } = "";

        public ExternalCredentialType ExternalCredentialType { get; private set; } = ExternalCredentialType.Epic;

        public AuthScopeFlags ScopeFlags
        {
            get
            {
                return AuthScopeFlags.BasicProfile | AuthScopeFlags.Presence | AuthScopeFlags.FriendsList;
            }
        }

        public void Initialize(Dictionary<string, string> commandLineArgs)
        {
            // Use command line arguments if passed
            ProductId = commandLineArgs.ContainsKey("-productid") ? commandLineArgs.GetValueOrDefault("-productid") : ProductId;
            SandboxId = commandLineArgs.ContainsKey("-sandboxid") ? commandLineArgs.GetValueOrDefault("-sandboxid") : SandboxId;
            DeploymentId = commandLineArgs.ContainsKey("-deploymentid") ? commandLineArgs.GetValueOrDefault("-deploymentid") : DeploymentId;
            ClientId = commandLineArgs.ContainsKey("-clientid") ? commandLineArgs.GetValueOrDefault("-clientid") : ClientId;
            ClientSecret = commandLineArgs.ContainsKey("-clientsecret") ? commandLineArgs.GetValueOrDefault("-clientsecret") : ClientSecret;
            LoginCredentialType = commandLineArgs.ContainsKey("-logincredentialtype") ? (LoginCredentialType)System.Enum.Parse(typeof(LoginCredentialType), commandLineArgs.GetValueOrDefault("-logincredentialtype")) : LoginCredentialType;
            Id = commandLineArgs.ContainsKey("-id") ? commandLineArgs.GetValueOrDefault("-id") : Id;
            Token = commandLineArgs.ContainsKey("-token") ? commandLineArgs.GetValueOrDefault("-token") : Token;
            ExternalCredentialType = commandLineArgs.ContainsKey("-externalcredentialtype") ? (ExternalCredentialType)System.Enum.Parse(typeof(ExternalCredentialType), commandLineArgs.GetValueOrDefault("-externalcredentialtype")) : ExternalCredentialType;
        }
    }
}
